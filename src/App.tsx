import { Router } from 'react-router';
import { createBrowserHistory } from 'history';
import { Root } from './views/Root';
import { UserContextProvider } from './contexts/UserContext/userContext';

const history = createBrowserHistory();

export const App = () => {
  return (
    <Router history={history}>
      <UserContextProvider>

        <Root/>
      </UserContextProvider>
    </Router>
  );
}

export default App;
