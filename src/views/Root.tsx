import React, { Fragment, useContext } from 'react';
import { Route, Switch } from 'react-router-dom';
// eslint-disable-next-line no-restricted-imports
import { Nav } from '../shared/components/layout/Nav/Nav';
// eslint-disable-next-line no-restricted-imports
import { UserContext } from '../contexts';
import { Panel } from './Panel';
import { Home } from './Home';
import { Login } from './Login';
import { Confirmation } from './Confirmation';

export const Root = () => {
  const { isLoggedIn } = useContext(UserContext)

  return (
    <Fragment>
      <Switch>
        {isLoggedIn ? (
          <Fragment>
            <Nav/>
            <Route exact path={'/panel'} component={Panel}/>
          </Fragment>
        )
          :   (
            <Fragment>
              <Nav/>
              <Route exact path={'/login'} component={Login}/>
              <Route exact path={'/'} component={Home}/>
              <Route exact path={'/confirmation'} component={Confirmation}/>

            </Fragment>
          )}
      </Switch>
    </Fragment>
  )
}