import { useCallback, useState } from 'react';
import { Controller } from 'react-hook-form';
import { useForm, FormProvider } from 'react-hook-form';
import { useHistory } from 'react-router';
import { ClipLoader } from 'react-spinners';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary, Button,
  Checkbox,
  FormControlLabel,
  Grid, TextField,
} from '@material-ui/core';
import { ExpandMore, Save } from '@material-ui/icons';
import { naturalDisasterImages } from '../config';
import { requestClient } from '../clients/request/requestClient';

export const Home = () => {
  const history = useHistory()
  const methods = useForm({
    defaultValues: {
      needs_firebrigade: false,
      needs_ambulance: false,
      disaster_type: '',
      phone_number: '',
    },
  });
  const { control, handleSubmit } = methods
  const [disasterType, setDisasterType] = useState('')
  const [isSending, setIsSending] = useState(false)

  const onSubmit = useCallback(async (formData: any) => {
    try {
      setIsSending(true)
      formData.disaster_type = disasterType
      await requestClient.postRequest(formData)
      console.log(formData)
      setIsSending(false)
      history.push('/confirmation')
    }
    catch (e) {
      throw(e)
    }
  }, [history, disasterType],
  )


  return (
    <FormProvider {...methods}>
      <form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
        <Grid container justify={'center'} direction={'row'} xs={12}>
          <Accordion style={{ width: '100%' }}>
            <AccordionSummary>
              <FormControlLabel
                label={'Potrzebna jest straż pożarna?'}
                control={
                  <Controller
                    control={control}
                    name="needs_firebrigade"
                    render={(props) => (
                      <Checkbox
                        {...props}
                        checked={props.value}
                        onChange={(e) => props.onChange(e.target.checked)}
                      />
                    )}
                  />
                }
              />
            </AccordionSummary>
          </Accordion>
          <Accordion style={{ width: '100%' }}>
            <AccordionSummary>
              <FormControlLabel
                label={'Potrzebne jest pogotowie?'}
                control={
                  <Controller
                    control={control}
                    name="needs_ambulance"
                    render={(props) => (
                      <Checkbox
                        {...props}
                        checked={props.value}
                        onChange={(e) => props.onChange(e.target.checked)}
                      />
                    )}
                  />
                }
              />
            </AccordionSummary>
          </Accordion>
          <Accordion style={{ width: '100%' }}>
            <AccordionSummary>
              <Controller
                name="phone_number"
                control={control}
                label={'Numer telefonu'}
                fullWidth
                as={<TextField/>}
              />
            </AccordionSummary>
          </Accordion>
          <Accordion>
            <AccordionSummary expandIcon={<ExpandMore/>}>
      Wybierz rodzaj klęski żywiołowej
            </AccordionSummary>
            <AccordionDetails>
              <Grid container spacing={3}>
                {naturalDisasterImages.map(({ src, label, value }: any, i:any) => (
                  <Grid justify={'center'} direction={'column'} key={i}>
                    <img src={src} title={label}></img>
                    <br/>
                    <FormControlLabel
                      label={label}
                      control={
                        <Controller
                          control={control}
                          name="disaster_type"
                          render={(props) => (
                            <Checkbox
                              {...props}
                              checked={disasterType === value}
                              onChange={(e) => [props.onChange(e.target.checked), setDisasterType(value)]}
                            />
                          )}
                        />
                      }
                    />
                  </Grid>

                ))}
              </Grid>
            </AccordionDetails>
          </Accordion>
        </Grid>
        <Grid style={{ float: 'right' }}>
          <Button
            variant="contained"
            color="primary"
            type="submit"
            size="large"
            startIcon={isSending ? <ClipLoader/> : <Save/>}
          > Utwórz zgłoszenie </Button>
        </Grid>
      </form>
    </FormProvider>
  )
}