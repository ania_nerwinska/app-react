import React, { useCallback, useContext } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router';
import { Button, Container, Grid, TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
// eslint-disable-next-line no-restricted-imports
import { UserContext } from '../contexts';


const useStyles = makeStyles((theme) => ({
  container: {
    position: 'relative',
    marginTop: 'calc(50vh - 200px)',
    overflow: 'visible',
  },
  logo: {
    textAlign: 'center',
    marginBottom: theme.spacing(4),
    '& > svg': {
      height: 100,
      width: 'auto',
    },
  },
  googleLogin: {
    marginTop: theme.spacing(4),
    textAlign: 'center',
    '& svg': {
      width: 40,
      height: 40,
      marginRight: theme.spacing(2),
    },
  },
  error: {
    color: 'red',
  },
}));


export const Login = () => {

  const classes = useStyles();
  const history = useHistory();
  const { register, handleSubmit, errors } = useForm({
    defaultValues: {
      username: '',
      password: '',
    },
  });
  const { login } = useContext(UserContext)

  const onSubmit = useCallback (async ({ username, password }) => {
    try {
      await login({ username, password });
      history.push('/panel')
    } catch (e){
      throw (e)
    }
  }, [login, history]);

  return (
    <Container maxWidth="xs" className={classes.container}>
      <div>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Grid container direction="column">
            <Grid item>
              <TextField
                fullWidth
                variant="outlined"
                label={'Username'}
                name="username"
                inputRef={register({ required: 'To pole jest wymagane' })}
              />
            </Grid>
            {errors.username && (
              <Grid item className={classes.error}>
                {errors.username.message}
              </Grid>
            )}
            <br/>
            <Grid item>
              <TextField
                fullWidth
                variant="outlined"
                label={'Hasło'}
                name="password"
                type="password"
                inputRef={register({ required: 'To pole jest wymagane' })}
              />
            </Grid>
            {errors.password && (
              <Grid item className={classes.error}>
                {errors.password.message}
              </Grid>
            )}
            <br/><br/>
            <Grid item>
              <Grid container justify="space-between" spacing={2}>
                <Grid item>
                  <Button size="large" type="submit" variant="contained" color="primary">
                    Login
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  )
}
