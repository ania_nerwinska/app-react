import { useContext } from 'react';
import { useQuery } from 'react-query';
import { UserContext } from '../contexts';
import { requestClient } from '../clients/request/requestClient';

export const Panel = () => {
  const { isLoggedIn } = useContext(UserContext)

  const { data: { data: requests = [] } = {} as any } = useQuery(
    'listRequests',
    requestClient.listRequests,
    { enabled: isLoggedIn },
  );
  console.log(requests)
  return (
    <div>aaa</div>
  )
}