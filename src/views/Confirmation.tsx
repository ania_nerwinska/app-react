import React, { Fragment } from 'react';
import { Grid, Typography } from '@material-ui/core';

export const Confirmation = () => {
  return (
    <Grid container justifyContent={'center'}>
      <Typography variant={'h5'}>
       Twoje zgłoszenie zostało zarejestrowane
      </Typography>
    </Grid>
  )
}