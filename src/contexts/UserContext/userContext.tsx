import React, { createContext, FC, useCallback, useEffect, useState } from 'react';
import { queryCache, useQuery } from 'react-query';
import { useHistory, useLocation } from 'react-router';
// eslint-disable-next-line no-restricted-imports
import { usersClient } from '../../clients/users/usersClient';
// eslint-disable-next-line no-restricted-imports
import { cacheKeys } from '../../config';


export const defaultContext: any = {
  isLoggedIn: false,
};

export const UserContext = createContext(defaultContext);
export const UserContextProvider: FC = ({ children }) => {
  const { search = '' } = useLocation();
  const params = new URLSearchParams(search);
  const token = params.get('token');
  const [isLoggedIn, setIsLoggedIn] = useState(!!token ? !!token : localStorage.getItem('token') !== null);


  const history = useHistory()

  const logout = useCallback( async () => {
    setIsLoggedIn(false);
    localStorage.removeItem('token');
    history.push('/')
  }, [history]);

  const login = useCallback(async (data) => {
    try {
      const { data: { token } } = await usersClient.login(data);
      localStorage.setItem('token', token);
      setIsLoggedIn(true);
      history.push('/panel')
    } catch (e){
      throw (e)
    }
  }, [history]);

  useEffect(() => {
    if(!!token) {
      localStorage.setItem('token', token);
    }
  }, [token])

  // @ts-ignore
  const { data: { data: userBackend = {} } = {} } = useQuery(
    'getMe',
    usersClient.getMe,
    { enabled: isLoggedIn },
  );

  return (
    <UserContext.Provider
      value={{
        userBackend,
        logout,
        login,
        isLoggedIn,
      }}
    >
      {children}
    </UserContext.Provider>
  );
}
