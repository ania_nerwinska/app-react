import queryString from 'query-string';
import axiosRequest, { AxiosResponse, AxiosRequestConfig } from 'axios';

export type AbstractRequest = (props: { options: AxiosRequestConfig; authenticate?: boolean; maxRetries?: number; }) => AxiosResponse;

const serializeParams = (params: any) => queryString.stringify(params);

const attachOptions = (options: AxiosRequestConfig, authenticate = true) => {
  const token = localStorage.getItem('token');
  return {
    ...options,
    paramsSerializer: (params: any) => serializeParams(params),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...options.headers,
      ...(token && authenticate && {
        'Authorization': 'Token ' + token,
      }),
    },
  };
}

//@ts-ignore
export const request: AbstractRequest = async ({ options }) => {
  const optionsWithHeader = await attachOptions(options);
  return axiosRequest(optionsWithHeader).catch((e) => {
    throw(e);
  });
}
