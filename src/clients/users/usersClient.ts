// eslint-disable-next-line no-restricted-imports
import { apiBaseUrl } from '../../config';
import { request } from '../baseClient';

const getMe = () => {
  return request({
    options: {
      url: `${apiBaseUrl}/user/me`,
      method: 'GET',
    },
  });
};

const login = (data: {email: string, password: string}) => {
  return request({
    options: {
      url: `${apiBaseUrl}/login/`,
      method: 'POST',
      data,
    },
  });
};

export const usersClient = {
  login,
  getMe,
};