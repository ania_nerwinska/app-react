import { apiBaseUrl } from '../../config';
import { request } from '../baseClient';

const postRequest = (data: any) => {
  return request({
    options: {
      url: `${apiBaseUrl}/request-create/`,
      method: 'POST',
      data,
    },
  });
};

const listRequests = () => {
  return request({
    options: {
      url: `${apiBaseUrl}/request`,
      method: 'GET',
    },
  });
};


const updateRequest = (id: number, data: any) => {
  return request({
    options: {
      url: `${apiBaseUrl}/request/${id}/`,
      method: 'PATCH',
      data,
    },
  });
};
export const requestClient = {
  postRequest,
  listRequests,
  updateRequest,
};
