export const cacheKeys = {
  getMe: 'getMe',
}

export const naturalDisasterImages = [
  { src: 'https://www.ang.pl/img/slownik/disaster.jpg', label: 'Katastrofa', value: 'katastrofa' },
  { src: 'https://www.ang.pl/img/slownik/earthquake.jpg', label: 'Trzesienie ziemi', value: 'trzesienie_ziemi' },
  { src: 'https://www.ang.pl/img/slownik/avalanche.jpg', label: 'Lawina', value: 'lawina' },
  { src: 'https://www.ang.pl/img/slownik/hailstorm.jpg', label: 'Gradobicie', value: 'gradobicie' },
  { src: 'https://www.ang.pl/img/slownik/twister.jpg', label: 'Trąba powietrzna', value: 'traba_powietrzna' },
  { src: 'https://www.ang.pl/img/slownik/blizzard.jpg', label: 'Zamieć', value: 'zamiec' },
  { src: 'https://www.ang.pl/img/slownik/flood.jpg', label: 'Powódź', value: 'powodz' },
  { src: 'https://www.ang.pl/img/slownik/fire.jpg', label: 'Pożar', value: 'pozar' },
]
export const apiBaseUrl = 'http://127.0.0.1:8000/backend/v1/api'
