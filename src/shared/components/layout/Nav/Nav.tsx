import { useContext } from 'react';
import { useHistory } from 'react-router';
import { AppBar, Toolbar, Button, Typography, makeStyles, Theme, createStyles } from '@material-ui/core';
// eslint-disable-next-line no-restricted-imports
import { UserContext } from '../../../../contexts';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
);

export const Nav = () => {
  const classes = useStyles();
  const history = useHistory();
  const { isLoggedIn, logout } = useContext(UserContext);

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" className={classes.title} onClick={() => {isLoggedIn ? history.push('/panel') : history.push('/')}}>
                    System kontroli zgłoszeń
        </Typography>
        {!isLoggedIn ? (
          <Button color="inherit" onClick={() => history.push('/login')}>Zaloguj</Button>
        ) : (
          <Button color="inherit" onClick={() => logout()}> Wyloguj </Button>
        )}
      </Toolbar>
    </AppBar>
  )
}